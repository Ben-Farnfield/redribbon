#Red Ribbon

A simple Android app built for the [Bradford University][1] society [Red Ribbon][2].

This app will provide society members with updates on upcoming and ongoing events.

###Docs

You can find the Javadoc for this in the [doc/][3] folder.

###Authors

This app has been written by the following members of PiSoc (website coming soon!):

* Ben Farnfield
* Rob Norvill

[1]: http://www.bradford.ac.uk/external/ "Bradford Uni home page"
[2]: http://www.bradford.ac.uk/students-union/student-activities/sports-and-societies/red-ribbon.php "Red Ribbon home page"
[3]: https://bitbucket.org/Ben-Farnfield/redribbon/src/84ab80f612391236d03958942b87a3f21a27388f/doc/?at=master "Javadoc"

